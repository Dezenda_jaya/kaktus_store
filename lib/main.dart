import 'package:flutter/material.dart';
import 'package:kaktus_shop/tab1.dart';
import 'package:kaktus_shop/tab2.dart';
import 'package:kaktus_shop/tab3.dart';
import 'package:kaktus_shop/my_profile.dart';



void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0.0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {},
        ),
        title: Text('Kaktus Shop',
            style: TextStyle(
                fontFamily: 'Nunito',
                fontSize: 25.0,
                color: Colors.white)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.account_box, color: Colors.white),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Profile())
              );},
          ),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.only(left: 20.0),
        children: <Widget>[

          SizedBox(height: 15.0),
          Text('        Silahkan Pilih Jenis Kaktus',
              style: TextStyle(
                  fontFamily: 'Nunito',
                  fontSize: 20.0,

                  fontWeight: FontWeight.bold)),
          SizedBox(height: 10.0),
          TabBar(
              controller: _tabController,
              indicatorColor: Colors.blue,
              labelColor: Colors.redAccent,
              isScrollable: true,
              labelPadding: EdgeInsets.only(right: 45.0),
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(
                  child: Text('Jenis 1',
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: 21.0,
                      )),
                ),
                Tab(
                  child: Text('Jenis 2',
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: 21.0,
                      )),
                ),
                Tab(
                  child: Text('Jenis 3',
                      style: TextStyle(
                        fontFamily: 'Nunito',
                        fontSize: 21.0,
                      )),
                )

              ]),
              Container(
                height: MediaQuery.of(context).size.height - 50.0,
                width: double.infinity,
                child: TabBarView(
                  controller: _tabController,
                  children: [
                    tab1(),
                    tab2(),
                    tab3(),
                  ]
                )
              )
        ],
      ),
      floatingActionButton: FloatingActionButton(onPressed: () {},
      backgroundColor: Colors.red,
      child: Icon(Icons.home),
      ),
    );
  }
}
