import 'package:kaktus_shop/detailpembayaran.dart';
import 'package:flutter/material.dart';
import 'package:kaktus_shop/main.dart';
import 'package:kaktus_shop/my_profile.dart';


class detailkaktus extends StatelessWidget {
  final assetPath, clockprice, clockname;

  detailkaktus({this.assetPath, this.clockprice, this.clockname});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0.0,
        centerTitle: true,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        title: Text('Kaktus Shop',
            style: TextStyle(
                fontFamily: 'Nunito',
                fontSize: 25.0,
                color: Colors.white)),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.account_box, color: Colors.white),
           onPressed: () {
             Navigator.push(context,
                 MaterialPageRoute(builder: (context) => Profile())
             );
              },//Bagian Ikon tentang Pembuat

          ),
        ],
      ),

      body: ListView(
          children: [
            SizedBox(height: 15.0),
            Padding(
              padding: EdgeInsets.only(left: 50.0),
              child: Text(
                  'Detail Jenis Kaktus',
                  style: TextStyle(
                      fontFamily: 'Nunito',
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black)
              ),
            ),
            SizedBox(height: 15.0),
            Hero(
                tag: assetPath,
                child: Image.asset(assetPath,
                    height: 150.0,
                    width: 100.0,
                    fit: BoxFit.contain
                )
            ),
            SizedBox(height: 20.0),
            Center(
              child: Text(clockprice,
                  style: TextStyle(
                      fontFamily: 'Nunito',
                      fontSize: 22.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black)),
            ),
            SizedBox(height: 10.0),
            Center(
              child: Text(clockname,
                  style: TextStyle(
                      color: Color(0xFF575E67),
                      fontFamily: 'Nunito',
                      fontSize: 24.0)),
            ),
            SizedBox(height: 20.0),

            SizedBox(height: 20.0),
            Center(

                child: Container(
                    width: MediaQuery.of(context).size.width - 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(40.0),
                        color: Colors.white70
                    ),
                    child: RaisedButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => InputData())
                          );
                        },

                    child: Center(
                        child: Text('Masukan ke Pembayaran',
                          style: TextStyle(
                              fontFamily: 'Nunito',
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black
                          ),

                          ),
                        )
                    )
                )
            )
          ]
      ),


      floatingActionButton: FloatingActionButton(onPressed: () {
        Navigator.push(context,
          MaterialPageRoute(builder: (context) => MyApp())
      );},
        backgroundColor: Colors.red,
        child: Icon(Icons.home),

      ),

    );
  }
}
